#include <VirtualWire.h>

const int led_pin = 13; 
const int transmit_pin = 12; 

char incomingchar;

void setup() {
       Serial.begin(9600);     
      vw_set_tx_pin(transmit_pin);
      vw_setup(2000);      
      pinMode(led_pin, OUTPUT);
}

void loop() {
       if (Serial.available()) {
               incomingchar = Serial.read();
                  if (incomingchar=='g'){
                   const char *msg = "g"; 
                   digitalWrite(led_pin, HIGH); 
                   vw_send((uint8_t *)msg, strlen(msg)); 
                   vw_wait_tx(); 
                   digitalWrite(led_pin, LOW); 
                   delay(1);} 

                  else if (incomingchar=='d'){
                   const char *msg ="d"; 
                   digitalWrite(led_pin, HIGH); 
                   vw_send((uint8_t *)msg, strlen(msg));
                   vw_wait_tx(); 
                   digitalWrite(led_pin, LOW);
                   delay(1);}

                  else if (incomingchar=='q'){
                   const char *msg = "q"; 
                   digitalWrite(led_pin, HIGH); 
                   vw_send((uint8_t *)msg, strlen(msg)); 
                   vw_wait_tx(); 
                   digitalWrite(led_pin, LOW); 
                   delay(1);} 

                  else if (incomingchar=='a'){
                   const char *msg = "a"; 
                   digitalWrite(led_pin, HIGH); 
                   vw_send((uint8_t *)msg, strlen(msg)); 
                   vw_wait_tx(); 
                   digitalWrite(led_pin, LOW); 
                   delay(1);} 

                  else if (incomingchar=='s'){
                   const char *msg = "s"; 
                   digitalWrite(led_pin, HIGH); 
                   vw_send((uint8_t *)msg, strlen(msg)); 
                   vw_wait_tx(); 
                   digitalWrite(led_pin, LOW); 
                   delay(1);}
                 else{
                 Serial.println(incomingchar);
       }
}
}